﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using appWeb.Models;

namespace appWeb.Controllers
{
    public class pratoesController : Controller
    {
        private RestauranteEntities db = new RestauranteEntities();

        // GET: pratoes
        public ActionResult Index()
        {
            var prato = db.prato.Include(p => p.restaurante);
            return View(prato.ToList());
        }


        [HttpPost]
        public ActionResult Index(string texto)
        {
            return View(db.prato.Where(x => x.pratoNome.Contains(texto)).OrderBy(x => x.pratoNome));
        }

        // GET: pratoes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            prato prato = db.prato.Find(id);
            if (prato == null)
            {
                return HttpNotFound();
            }
            return View(prato);
        }

        // GET: pratoes/Create
        public ActionResult Create()
        {
            ViewBag.restauranteID = new SelectList(db.restaurante, "restauranteID", "restauranteNome");
            return View();
        }

        // POST: pratoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "pratoID,pratoNome,restauranteID,valor")] prato prato)
        {
            if (ModelState.IsValid)
            {
                db.prato.Add(prato);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.restauranteID = new SelectList(db.restaurante, "restauranteID", "restauranteNome", prato.restauranteID);
            return View(prato);
        }

        // GET: pratoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            prato prato = db.prato.Find(id);
            if (prato == null)
            {
                return HttpNotFound();
            }
            ViewBag.restauranteID = new SelectList(db.restaurante, "restauranteID", "restauranteNome", prato.restauranteID);
            return View(prato);
        }

        // POST: pratoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "pratoID,pratoNome,restauranteID,valor")] prato prato)
        {
            if (ModelState.IsValid)
            {
                db.Entry(prato).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.restauranteID = new SelectList(db.restaurante, "restauranteID", "restauranteNome", prato.restauranteID);
            return View(prato);
        }

        // GET: pratoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            prato prato = db.prato.Find(id);
            if (prato == null)
            {
                return HttpNotFound();
            }
            return View(prato);
        }

        // POST: pratoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            prato prato = db.prato.Find(id);
            db.prato.Remove(prato);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
